class Footer {
    create (){
        const footerElement = document.createElement ('footer');
        footerElement.classList.add('footer');
        footerElement.innerHTML = `<div class = "container">
                                        <div class = "footer__wrapper">
                                            <div class = "footer__contact">
                                                <div class="footer__contact__email">
                                                    <div class="footer__contact__email__tittle">Email</div>
                                                    <div class="footer__contact__email__link"><a href="mailto:order@technoworld.com">order@technoworld.com</a></div>
                                                </div>
                                                <div class="footer__contact__phone">
                                                    <div class="footer__contact__phone__tittle">Phone</div>
                                                    <div class="footer__contact__phone__link"><a href="tel:+375297777777">+375(29)7777777</a></div>
                                                </div>
                                                <div class="footer__contact__address">
                                                    <div class="footer__contact__address__tittle">Address</div>
                                                    <div class="footer__contact__address__link"> <a href="https://goo.gl/maps/kZ62foJgRhZB9tUV6" target="_blank">г.Минск ул.Домбровская 9</a></div>
                                                </div>
                                            </div>
                                            <div class= "footer__networks__wrapper">
                                                <div class = "footer__networks__wrapper__tittle">TechnoWorld</div>
                                                <div class="footer__networks">
                                                    <a href="#" class="footer__networks__link"> 
                                                        <div class="footer__networks__bg fb__link"></div> 
                                                    </a> 
                                                    <a href="#" class="footer__networks__link"> 
                                                        <div class="footer__networks__bg twitter__link"></div> 
                                                    </a> 
                                                    <a href="#" class="footer__networks__link"> 
                                                        <div class="footer__networks__bg insta__link"></div> 
                                                    </a> 
                                                </div>
                                            </div>
                                            <div class = "footer__logo">
                                                <a href = "#">
                                                    <img src= "../img/logo.png">
                                                </a>
                                            </div>
                                        </div>
                                    </div>`

        return footerElement;
    }
}

const footer = new Footer();

export default footer;

