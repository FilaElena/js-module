const metaCharSet = document.createElement('meta');
metaCharSet.setAttribute('charset', 'UTF-8');
document.head.appendChild(metaCharSet);

const metaViewPort = document.createElement('meta');
metaViewPort.setAttribute('name', 'viewport');
metaViewPort.setAttribute('content', 'width=device-width, initial-scale=1.0');
document.head.appendChild(metaViewPort);

const title = document.createElement('title');
title.innerHTML = 'Default title';
document.head.appendChild(title);

const cssLink = document.createElement('link');
cssLink.setAttribute('rel', 'stylesheet');
cssLink.setAttribute('href', './css/style.css');
document.head.appendChild(cssLink);

const mediaLink = document.createElement('link');
mediaLink.setAttribute('rel', 'stylesheet');
mediaLink.setAttribute('href', './css/media.css');
document.head.appendChild(mediaLink);

const fontFLink = document.createElement('link');
fontFLink.setAttribute('rel', 'preconnect');
fontFLink.setAttribute('href', 'https://fonts.googleapis.com');
document.head.appendChild(fontFLink);

const fontSLink = document.createElement('link');
fontSLink.setAttribute('rel', 'preconnect');
fontSLink.setAttribute('href', 'https://fonts.gstatic.com');
fontSLink.setAttribute('crossorigin', '');
document.head.appendChild(fontSLink);

const fontTLink = document.createElement('link');
fontTLink.setAttribute('rel', 'stylesheet');
fontTLink.setAttribute('href', 'https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,700;1,400&display=swap');
document.head.appendChild(fontTLink);

